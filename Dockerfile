FROM python:3.9.7

MAINTAINER MAX NOVOSELSKY <relocker121@gmail.com>

RUN apt-get update

COPY smartrasp_server /usr/src/smartrasp_server

WORKDIR "/usr/src/smartrasp_server/"

RUN pip install -r requirements.txt

EXPOSE 8000

CMD ["gunicorn", "smartrasp_server.wsgi", "--bind", "0.0.0.0"]