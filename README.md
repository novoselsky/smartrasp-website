# README #

## What is this repository for? ##

This is an alternative website for Tomsk Polytechnic University schedule with filter system.

All project based on Django Framework.

## Installation and deployment ##

### Dependencies ###

python 3.8+, Django 3.2

### Basic installation ###

Clone this repo:

    git clone git@bitbucket.org:novoselsky/smartrasp-website.git

Install dependencies:

    pip install -r requirements.txt

After that you need to create your database. To do it, run next command in smartrasp_server directory:

    python manage.py makemigrations
    python manage.py migrate

Then run 

    python manage.py runserver

and go to [127.0.0.1:8000](http://127.0.0.1:8000/).

If everything is OK, you will see the welcome page.

### Adding some lessons ###

After basic installation you will have your website without any lesson's data.

You can add lessons using json file with next structure:

    [
        {
            'teacher_name': 'James Smith',
            'full_name': 'Electronic',
            'short_name': 'Electr',
            'date': '09.08.21',
            'lesson_number': 2,
            'group_name': '8V92',
        }
    ]

Date should be in dd.mm.yy format.

For adding lessons from json file run in smartrasp_server directory:

    python manage.py add_lessons path/to/json_file.json

Smartrasp website will automatically create all nonexistent groups, teachers and subjects, and then add lessons with
this them.

#### Admin rights ####

Django provides basic admin panel, where you can see and modifying all existed data. For entering this panel you need to
create a user with admin rights. You can do it via next command:

    python manage.py createsuperuser

Then simply go to [127.0.0.1:8000/admin](http://127.0.0.1:8000/admin) while server is running.


### Using Docker ###

You can run this website using docker and docker-compose. This is the easiest and recommendable way to run website on
actual server.

You can see how to install docker for different OS [here](https://docs.docker.com/engine/install/). (you need 1.13.0+
version)

Then you can run your server simply using next commands:

    sudo docker-compose build smartrasp-server
    sudo docker-compose run -d --publish 8080:8000 smartrasp-server

This will run website on 8080 port. If you need different port, just change 8080 to your port in second command.

### Collecting static ###

Running site with docker will forbid you getting static files, such as css and js files. You need to open them to users
using third party web-server (for example, I'm using [nginx](https://nginx.org)).

All static files stored in smartrasp_server/static/ folder.

## Repo owner info ##

Yo! My name is Max Novoselsky :)

You can contact with me via this email: relocker121@gmail.com