from .models import BaseFilter


def get_user_filters_list(schedule):
    filters_classes = BaseFilter.__subclasses__()
    user_filters = []
    for FilterClass in filters_classes:
        filters = list(FilterClass.objects.filter(schedule=schedule))
        user_filters += filters
    return user_filters


def filter_for_user(schedule, lessons_set):
    user_filters = get_user_filters_list(schedule)
    common_lessons = lessons_set
    shadow_lessons = lessons_set.none()
    for filter_object in user_filters:
        common_lessons, filtered_shadow_lessons = filter_object.filter(
                                                        common_lessons)
        shadow_lessons = shadow_lessons.union(filtered_shadow_lessons)
    return common_lessons, shadow_lessons
