from django.apps import AppConfig


class MeetingscreenConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'meetingScreen'
