from django.test import TestCase
from .models import Group, Lesson, Subject
from .models import FilterSubjectName
from .models import Schedule
from django.contrib.auth.models import User

import datetime

from .week_handler import get_lessons_for_day_and_lesson_number
from .week_handler import get_lessons_for_day_date
from .week_handler import get_lessons_for_week
from .week_handler import get_week_lessons_for_view_wo_times
from .week_handler import get_week_lessons_for_view
from .week_handler import get_filtered_lessons_for_week

from .filter_handler import get_user_filters_list
from .filter_handler import filter_for_user


# Create your tests here.

def set_up_basic_db(self):
    self.main_group = Group.objects.create(name="testGroup")
    self.date1 = datetime.date(2021, 11, 3)
    self.date2 = datetime.date(2021, 11, 5)
    self.user = User.objects.create_user(username="test_user",
                                         password="12345")
    self.schedule = Schedule.objects.create(main_group=self.main_group,
                                            schedule_type=Schedule.STUDENT,
                                            django_user_ref=self.user)
    self.subject = Subject.objects.create(full_name="test_subject",
                                          short_name="s")
    self.subject2 = Subject.objects.create(full_name="test_subject2",
                                           short_name="s2")
    self.lesson = Lesson.objects.create(subject=self.subject,
                                        group=self.main_group,
                                        lesson_number=2,
                                        date=self.date1)
    self.lesson2 = Lesson.objects.create(subject=self.subject,
                                         group=self.main_group,
                                         lesson_number=2,
                                         date=self.date2)
    self.lesson3 = Lesson.objects.create(subject=self.subject2,
                                         group=self.main_group,
                                         lesson_number=1,
                                         date=self.date1)


class FilterInterfaceTestCase(TestCase):
    def setUp(self):
        set_up_basic_db(self)
        self.filter1 = FilterSubjectName.objects.create(
            schedule=self.schedule, subject=self.subject,
            type=FilterSubjectName.HIDE
        )
        self.filter2 = FilterSubjectName.objects.create(
            schedule=self.schedule, subject=self.subject,
            type=FilterSubjectName.SHADOW
        )

    def test_filter_hide(self):
        input = Lesson.objects.filter(date=self.date1)
        self.assertNotEqual(input, [self.lesson, self.lesson3])
        self.assertEqual(list(input), [self.lesson, self.lesson3])
        true_output = [self.lesson3]
        test_output, empty_output = self.filter1.filter_hide(input)
        self.assertNotEqual(empty_output, [])
        self.assertNotEqual(test_output, [])
        self.assertEqual(list(empty_output), [])
        self.assertEqual(list(test_output), true_output)

    def test_filter_shadow(self):
        input = Lesson.objects.filter(date=self.date1)
        self.assertNotEqual(input, [self.lesson, self.lesson3])
        self.assertEqual(list(input), [self.lesson, self.lesson3])
        true_common_output = [self.lesson3]
        true_shadow_output = [self.lesson]
        test_common_output, test_shadow_output = self.filter1.filter_shadow(input)
        self.assertNotEqual(test_common_output, true_common_output)
        self.assertNotEqual(test_shadow_output, true_shadow_output)
        self.assertEqual(list(test_common_output), true_common_output)
        self.assertEqual(list(test_shadow_output), true_shadow_output)

    def test_filter(self):
        input = Lesson.objects.filter(date=self.date1)
        true_common_output1 = [self.lesson3]
        true_common_output2 = [self.lesson3]
        true_shadow_output1 = []
        true_shadow_output2 = [self.lesson]
        test_common_output1, test_shadow_output1 = self.filter1.filter(input)
        test_common_output2, test_shadow_output2 = self.filter2.filter(input)
        self.assertNotEqual(test_common_output1, true_common_output1)
        self.assertNotEqual(test_common_output2, true_common_output2)
        self.assertNotEqual(test_shadow_output1, true_shadow_output1)
        self.assertNotEqual(test_shadow_output2, true_shadow_output2)
        self.assertEqual(list(test_common_output1), true_common_output1)
        self.assertEqual(list(test_common_output2), true_common_output2)
        self.assertEqual(list(test_shadow_output1), true_shadow_output1)
        self.assertEqual(list(test_shadow_output2), true_shadow_output2)


class FiltersTestCase(TestCase):
    def setUp(self):
        set_up_basic_db(self)
        self.filter1 = FilterSubjectName.objects.create(
            schedule=self.schedule, subject=self.subject,
            type=FilterSubjectName.HIDE
        )
        self.filter2 = FilterSubjectName.objects.create(
            schedule=self.schedule, subject=self.subject2,
            type=FilterSubjectName.SHADOW
        )
        self.subject3 = Subject.objects.create(full_name="test_subject3",
                                               short_name="s3")
        self.lesson4 = Lesson.objects.create(subject=self.subject3,
                                             group=self.main_group,
                                             lesson_number=2,
                                             date=self.date1)

    def test_get_user_filters_list(self):
        true_output = [self.filter1, self.filter2]
        test_output = get_user_filters_list(self.schedule)
        self.assertEqual(test_output, true_output)

    def test_filter_for_user(self):
        input = Lesson.objects.all()
        true_common = [self.lesson4]
        true_shadow = [self.lesson3]
        test_common, test_shadow = filter_for_user(self.schedule, input)
        self.assertNotEqual(test_common, true_common)
        self.assertNotEqual(test_shadow, true_shadow)
        self.assertEqual(list(test_common), true_common)
        self.assertEqual(list(test_shadow), true_shadow)


class WeekHandlerTestCase(TestCase):
    def setUp(self):
        set_up_basic_db(self)

    def test_get_lessons_for_day_and_lesson_number(self):
        lessons1 = get_lessons_for_day_and_lesson_number(self.schedule,
                                                         datetime.date(2021, 11, 3),
                                                         2)
        lessons2 = get_lessons_for_day_and_lesson_number(self.schedule,
                                                         datetime.date(2021, 11, 3),
                                                         3)
        self.assertEqual(list(lessons1), [self.lesson])
        self.assertEqual(list(lessons2), [])

    def test_get_lessons_for_day_date_empty(self):
        day = get_lessons_for_day_date(self.schedule, datetime.date(2021, 9, 1))
        day = list(map(lambda lessons_set: list(lessons_set), day))
        true_result = [[] for _ in range(7)]
        self.assertEqual(day, true_result)

    def test_get_lessons_for_day_date_with_lessons(self):
        day = get_lessons_for_day_date(self.schedule, datetime.date(2021, 11, 3))
        day = list(map(lambda lessons_set: list(lessons_set), day))
        true_result = [[self.lesson3],
                       [self.lesson]]
        for i in range(5):
            true_result.append([])
        self.assertEqual(day, true_result)

    def test_get_lessons_for_week_empty(self):
        week = get_lessons_for_week(self.schedule, 10)
        true_result = [[[] for _ in range(7)] for _ in range(6)]
        for day_index in range(len(week)):
            day_set = week[day_index]
            day_map = map(lambda lessons_set: list(lessons_set), day_set)
            week[day_index] = list(day_map)
        self.assertEqual(week, true_result)

    def test_get_lessons_for_week_with_lessons(self):
        week = get_lessons_for_week(self.schedule, 9)
        true_result = [
            [[] for _ in range(7)],  # MONDAY (2021, 11, 1)
            [[] for _ in range(7)],  # TUESDAY (2021, 11, 2)
            [[self.lesson3], [self.lesson],
             [], [], [], [], []],  # WEDNESDAY (2021, 11, 3)
            [[] for _ in range(7)],  # THURSDAY (2021, 11, 4)
            [[], [self.lesson2],
             [], [], [], [], []],  # FRIDAY (2021, 11, 5)
            [[] for _ in range(7)]  # SATURDAY (2021, 11, 6)
        ]
        for day_index in range(len(week)):
            day_set = week[day_index]
            day_map = map(lambda lessons_set: list(lessons_set), day_set)
            week[day_index] = list(day_map)
        self.assertEqual(week, true_result)

    def test_get_week_lessons_for_view_wo_times_empty(self):
        true_result = [
            [[] for _ in range(6)],  # FIRST LESSON
            [[] for _ in range(6)],  # SECOND LESSON
            [[] for _ in range(6)],
            [[] for _ in range(6)],
            [[] for _ in range(6)],
            [[] for _ in range(6)],
            [[] for _ in range(6)]
        ]
        my_result = get_week_lessons_for_view_wo_times(self.schedule, 1)
        for lesson_num in range(len(my_result)):
            day_set = my_result[lesson_num]
            day_map = map(lambda les: list(les), day_set)
            my_result[lesson_num] = list(day_map)
        self.assertEqual(my_result, true_result)

    def test_get_week_lessons_for_view_wo_times_with_lessons(self):
        true_result = [
            [[], [], [self.lesson3], [], [], []],  # FIRST LESSON
            [[], [], [self.lesson], [], [self.lesson2], []],  # SECOND LESSON
            [[] for _ in range(6)],
            [[] for _ in range(6)],
            [[] for _ in range(6)],
            [[] for _ in range(6)],
            [[] for _ in range(6)]
        ]
        my_result = get_week_lessons_for_view_wo_times(self.schedule, 9)
        for lesson_num in range(len(my_result)):
            day_set = my_result[lesson_num]
            day_map = map(lambda les: list(les), day_set)
            my_result[lesson_num] = list(day_map)
        self.assertEqual(my_result, true_result)

    def test_get_week_lessons_for_view_empty(self):
        true_result = [
            [
                '8:30<br><span class="second-time">10:05</span>',
                '10:25<br><span class="second-time">12:00</span>',
                '12:40<br><span class="second-time">14:15</span>',
                '14:35<br><span class="second-time">16:10</span>',
                '16:30<br><span class="second-time">18:05</span>',
                '18:25<br><span class="second-time">20:00</span>',
                '20:20<br><span class="second-time">21:55</span>'
            ],
            [
                [[] for _ in range(6)],
                [[] for _ in range(6)],
                [[] for _ in range(6)],
                [[] for _ in range(6)],
                [[] for _ in range(6)],
                [[] for _ in range(6)],
                [[] for _ in range(6)]
            ]
        ]
        # Don't forget to unzip result
        my_result = list(zip(*get_week_lessons_for_view(self.schedule, 1)))
        my_result = list(map(lambda x: list(x), my_result))
        for lesson_num in range(len(my_result[1])):
            day_set = my_result[1][lesson_num]
            day_map = map(lambda les: list(les), day_set)
            my_result[1][lesson_num] = list(day_map)
        self.assertEqual(true_result, my_result)

    def test_get_week_lessons_for_view_with_lessons(self):
        true_result = [
            [
                '8:30<br><span class="second-time">10:05</span>',
                '10:25<br><span class="second-time">12:00</span>',
                '12:40<br><span class="second-time">14:15</span>',
                '14:35<br><span class="second-time">16:10</span>',
                '16:30<br><span class="second-time">18:05</span>',
                '18:25<br><span class="second-time">20:00</span>',
                '20:20<br><span class="second-time">21:55</span>'
            ],
            [
                [[], [], [self.lesson3], [], [], []],
                [[], [], [self.lesson], [], [self.lesson2], []],
                [[] for _ in range(6)],
                [[] for _ in range(6)],
                [[] for _ in range(6)],
                [[] for _ in range(6)],
                [[] for _ in range(6)]
            ]
        ]
        # Don't forget to unzip result
        my_result = list(zip(*get_week_lessons_for_view(self.schedule, 9)))
        my_result = list(map(lambda x: list(x), my_result))
        self.assertNotEqual(my_result, true_result)
        for lesson_num in range(len(my_result[1])):
            day_set = my_result[1][lesson_num]
            day_map = map(lambda les: list(les), day_set)
            my_result[1][lesson_num] = list(day_map)
        self.assertEqual(my_result, true_result)

    def test_get_filtered_lessons_for_week(self):
        self.subject3 = Subject.objects.create(full_name="test_subject3",
                                               short_name="s3")
        self.lesson4 = Lesson.objects.create(subject=self.subject3,
                                             group=self.main_group,
                                             lesson_number=2,
                                             date=self.date1)
        FilterSubjectName.objects.create(
            schedule=self.schedule, subject=self.subject,
            type=FilterSubjectName.HIDE
        )
        FilterSubjectName.objects.create(
            schedule=self.schedule, subject=self.subject2,
            type=FilterSubjectName.SHADOW
        )
        """
        [
            [[] for _ in range(7)],  # MONDAY (2021, 11, 1)
            [[] for _ in range(7)],  # TUESDAY (2021, 11, 2)
            [[self.lesson3], [self.lesson],
             [], [], [], [], []],  # WEDNESDAY (2021, 11, 3)
            [[] for _ in range(7)],  # THURSDAY (2021, 11, 4)
            [[], [self.lesson2],
             [], [], [], [], []],  # FRIDAY (2021, 11, 5)
            [[] for _ in range(7)]  # SATURDAY (2021, 11, 6)
        ]
        """
        true_common = [
            [[] for _ in range(7)],
            [[] for _ in range(7)],
            [[], [self.lesson4], [], [], [], [], []],
            [[] for _ in range(7)],
            [[] for _ in range(7)],
            [[] for _ in range(7)],
        ]
        true_shadow = [
            [[] for _ in range(7)],
            [[] for _ in range(7)],
            [[self.lesson3], [], [], [], [], [], []],
            [[] for _ in range(7)],
            [[] for _ in range(7)],
            [[] for _ in range(7)],
        ]
        test_common, test_shadow = get_filtered_lessons_for_week(
            self.schedule, 9)
        self.assertNotEqual(test_common, true_common)
        self.assertNotEqual(test_shadow, true_shadow)
        for day_index in range(len(test_common)):
            day_set = test_common[day_index]
            day_map = map(lambda lessons_set: list(lessons_set), day_set)
            test_common[day_index] = list(day_map)
        for day_index in range(len(test_shadow)):
            day_set = test_shadow[day_index]
            day_map = map(lambda lessons_set: list(lessons_set), day_set)
            test_shadow[day_index] = list(day_map)
        self.assertEqual(test_common, true_common)
        self.assertEqual(test_shadow, true_shadow)