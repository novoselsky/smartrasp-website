let colors = {
    "ПР": "rgba(98, 168, 98, 0.2)", // green
    "ЛБ": "rgba(119, 87, 161, 0.2)", // purple
    "ЛК": "rgba(75, 153, 179, 0.2)"  // yellow
};

$(document).ready(function() {
    // set background
    $('.table td').each(function () {
        if ($(this).find(".lesson-type").length) {
            let bg_color = colors[$(this).find(".lesson-type")[0].innerHTML];
            $(this).css("background-color", bg_color);
        };
        // and delete unnecessary (last) hr
        if ($(this).find("hr").length) {
            let last_hr = $(this).find("hr")[$(this).find("hr").length - 1];
            last_hr.remove();
        };
    });
    // Delete unnecessary br tags, if lesson has no teacher
    $('.lesson-teacher').each(function (){
        if (this.innerHTML === "<br>") {
            this.remove();
        };
    });
})