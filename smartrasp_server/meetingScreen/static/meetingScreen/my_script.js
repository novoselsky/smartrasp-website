$(document).ready(function() {
    $('#mySelect2').select2({
        dropdownAutoWidth : true,
        width: "100%"
    });
    $('#mySelect3').select2({
        dropdownAutoWidth : true,
        width: "100%"
    });
    // If we would add this styles in css file
    // they wouldn't display, because select2
    // changing styles while init
    $(".select2-container--default .select2-selection--single").css("background-color", "#202124");
    $(".select2-container--default .select2-selection--single .select2-selection__rendered").css("color", "#bdc1c6");
    $(".select2-container--default .select2-selection--single .select2-selection__rendered").css("box-shadow", "1px 1px 15px 0px #171717");
    // We need to open and close our selects
    // to fix incorrect position showing
    // in a first open. It's this bug:
    // https://github.com/select2/select2/issues/3228
    $('#mySelect2').select2('open');
    $('#mySelect2').select2('close');
    $('#mySelect3').select2('open');
    $('#mySelect3').select2('close');
    $('#mylist').select2({
    sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
    });
});

$(document).on('select2:open', () => {
document.querySelector('.select2-search__field').focus();
});