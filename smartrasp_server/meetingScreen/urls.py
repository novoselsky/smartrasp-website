from django.urls import path

from . import views

app_name = 'meetingScreen'
urlpatterns = [
    path('', views.index_page, name='index'),
    path('table', views.table_current_week, name='table_cur_week'),
    path('table/<int:week_number>', views.table_page, name='table'),
    path('new_student', views.create_new_student, name='new_student'),
    path('new_teacher', views.create_new_teacher, name='new_teacher'),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('new_filter', views.create_new_filter, name='new_filter'),
    path('settings', views.settings_page, name='settings_page'),
    path('change_password_action', views.change_password_action,
         name='change_password_action'),
]
