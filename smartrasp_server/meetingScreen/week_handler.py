import datetime
from math import ceil
from .models import Lesson
from .filter_handler import filter_for_user

first_monday = datetime.date(2022, 8, 29)
day_delta = datetime.timedelta(days=1)
week_delta = datetime.timedelta(days=7)


def get_user_schedule_type_filter(user):
    if user.schedule_type == user.STUDENT:
        filter_type = "group"
        filter_value = user.main_group
    else:
        filter_type = "teacher"
        filter_value = user.main_teacher
    return {filter_type: filter_value}


def get_lessons_for_day_and_lesson_number(user, day_date, lesson_num):
    filter_dict = get_user_schedule_type_filter(user)
    filter_dict.update({'date': day_date,
                        'lesson_number': lesson_num})
    return Lesson.objects.filter(**filter_dict)


def get_lessons_for_day_date(user, day_date):
    lessons = [get_lessons_for_day_and_lesson_number(user, day_date, i)
               for i in range(1, 8)]
    return lessons


def get_lessons_for_week(user, week_number):
    """Weeks starts with zero!"""
    monday = first_monday + (week_number * week_delta)
    lessons = []
    for day_num in range(6):
        day_date = monday + (day_delta * day_num)
        lessons.append(get_lessons_for_day_date(user, day_date))
    return lessons


def get_filtered_lessons_for_week(user, week_number):
    lessons_for_week = get_lessons_for_week(user, week_number)
    common = []
    shadow = []
    for day_num in range(6):
        common.append([])
        shadow.append([])
        for les_num in range(7):
            common_set, shadow_set = filter_for_user(user,
                                    lessons_for_week[day_num][les_num])
            common[day_num].append(common_set)
            shadow[day_num].append(shadow_set)
    return common, shadow


def get_week_lessons_for_view_wo_times(user, week_number):
    lessons = get_lessons_for_week(user, week_number)
    lessons_transpose = [list(x) for x in zip(*lessons)]
    return lessons_transpose


def get_filtered_week_lessons_for_view_wo_times(user, week_number):
    common, shadow = get_filtered_lessons_for_week(user, week_number)
    common_transpose = [list(x) for x in zip(*common)]
    shadow_transpose = [list(x) for x in zip(*shadow)]
    return common_transpose, shadow_transpose


# TODO you can add times to week lessons using zip(times, *lessons)


def get_week_lessons_for_view(user, week_number):
    """
    It's much easier to render template, when week lessons info,
    having next structure: zip(time_arr, lessons_arr), where
    time_arr is [time1, time2, ..., time7]
    lessons_arr is [first_lessons_arr, ..., seventh_lessons_arr]
    first_lessons_arr is [monday_first_lesson_set, ...,
                            saturday_first_lessons_set]
    """
    times = [
        '8:30<br><span class="second-time">10:05</span>',
        '10:25<br><span class="second-time">12:00</span>',
        '12:40<br><span class="second-time">14:15</span>',
        '14:35<br><span class="second-time">16:10</span>',
        '16:30<br><span class="second-time">18:05</span>',
        '18:25<br><span class="second-time">20:00</span>',
        '20:20<br><span class="second-time">21:55</span>'
    ]
    lessons = get_week_lessons_for_view_wo_times(user, week_number)
    return zip(times, lessons)


def get_filtered_week_lessons_for_view(user, week_number):
    times = [
        '8:30<br><span class="second-time">10:05</span>',
        '10:25<br><span class="second-time">12:00</span>',
        '12:40<br><span class="second-time">14:15</span>',
        '14:35<br><span class="second-time">16:10</span>',
        '16:30<br><span class="second-time">18:05</span>',
        '18:25<br><span class="second-time">20:00</span>',
        '20:20<br><span class="second-time">21:55</span>'
    ]
    common, shadow = get_filtered_week_lessons_for_view_wo_times(user,
                                                                 week_number)
    return zip(times, common, shadow)


def get_week_dates(week_number):
    """Return list with week dates in string format."""
    monday = first_monday + (week_number * week_delta)
    week_days_dates = []
    for day_number in range(6):
        day = monday + (day_number * day_delta)
        week_days_dates.append(str(day))
    return week_days_dates


def get_week_days_with_dates(week_number):
    """Return zip with days names and dates"""
    days = [
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота',
    ]
    dates = get_week_dates(week_number)
    days_and_dates = zip(days, dates)
    return days_and_dates


def get_current_week():
    today = datetime.datetime.date(datetime.datetime.today())
    return ceil((today - first_monday)/week_delta)
