from django.core.management.base import BaseCommand, CommandError
import json
import datetime
from meetingScreen.models import Lesson, Teacher, Subject, Group
from django.db import IntegrityError


def get_teacher_or_create_if_not_exist(teacher_name):
    if teacher_name is None:
        return None
    try:
        teacher = Teacher.objects.get(full_name=teacher_name)
    except Teacher.DoesNotExist:
        teacher = Teacher.objects.create(full_name=teacher_name)
    return teacher


def get_subject_or_create_if_not_exist(full_name, short_name):
    try:
        subject = Subject.objects.get(full_name=full_name)
    except Subject.DoesNotExist:
        subject = Subject.objects.create(full_name=full_name,
                                         short_name=short_name)
    return subject


def get_group_or_create_if_not_exist(group_name):
    try:
        group = Group.objects.get(name=group_name)
    except Group.DoesNotExist:
        group = Group.objects.create(name=group_name)
    return group


"""
lesson_dict example:
{
    'teacher_name': 'James Smith',
    'full_name': 'Electronic',
    'short_name': 'Electr',
    'date': '09.08.21',         # dd.mm.yy
    'lesson_number': 2,
    'group_name': '8V92',
    'lesson_type': 'ПР',
}
"""


def create_lesson_from_dict(lesson_dict):
    date = datetime.datetime.strptime(lesson_dict['date'], '%d.%m.%y')
    date = date.date()
    teacher = get_teacher_or_create_if_not_exist(lesson_dict['teacher_name'])
    subject = get_subject_or_create_if_not_exist(lesson_dict['full_name'],
                                                 lesson_dict['short_name'])
    group = get_group_or_create_if_not_exist(lesson_dict['group_name'])
    lesson_number = lesson_dict['lesson_number']
    lesson_type = lesson_dict['lesson_type']
    try:
        Lesson.objects.get(subject=subject, group=group, teacher=teacher,
                           lesson_number=lesson_number, date=date,
                           type=lesson_type)
    except Lesson.DoesNotExist:
        Lesson.objects.create(subject=subject, teacher=teacher, date=date,
                              lesson_number=lesson_number, group=group,
                              type=lesson_type)


def append_lessons_from_json_file(file_pth):
    with open(file_pth, 'r', encoding='utf-8') as file:
        lesson_dict_list = json.load(file)
        lessons_appended = 0
        lessons_amount = len(lesson_dict_list)
        for lesson_dict in lesson_dict_list:
            try:
                create_lesson_from_dict(lesson_dict)
            except IntegrityError:
                continue
            lessons_appended += 1
            print(f"Appended {lessons_appended}/{lessons_amount} lessons")


class Command(BaseCommand):
    help = "Import lesson from json file to database"

    def add_arguments(self, parser):
        parser.add_argument('json_path', type=str)

    def handle(self, *args, **options):
        try:
            append_lessons_from_json_file(options['json_path'])
            self.stdout.write(self.style.SUCCESS("Success"))
        except FileNotFoundError:
            self.stdout.write(self.style.ERROR(f"No such file {options['json_path']}"))
