from csv import DictReader

from django.core.management.base import BaseCommand, CommandError
import datetime
from meetingScreen.models import Lesson, Teacher, Subject, Group
from django.db import IntegrityError


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


teachers_map = {}
subjects_map = {}
groups_map = {}

def get_teacher_or_create_if_not_exist(teacher_name):
    if teacher_name in teachers_map:
        return teachers_map[teacher_name]

    if teacher_name is None:
        return None
    try:
        teacher = Teacher.objects.get(full_name=teacher_name)
    except Teacher.DoesNotExist:
        teacher = Teacher.objects.create(full_name=teacher_name)

    teachers_map[teacher_name] = teacher
    return teacher


def get_subject_or_create_if_not_exist(full_name, short_name):
    if full_name in subjects_map:
        return subjects_map[full_name]

    try:
        subject = Subject.objects.get(full_name=full_name)
    except Subject.DoesNotExist:
        subject = Subject.objects.create(full_name=full_name,
                                         short_name=short_name)

    subjects_map[full_name] = subject
    return subject


def get_group_or_create_if_not_exist(group_name):
    if group_name in groups_map:
        return groups_map[group_name]

    try:
        group = Group.objects.get(name=group_name)
    except Group.DoesNotExist:
        group = Group.objects.create(name=group_name)

    groups_map[group_name] = group
    return group


"""
lesson_dict example:
{
    'teacher_name': 'James Smith',
    'full_name': 'Electronic',
    'short_name': 'Electr',
    'date': '09.08.21',         # dd.mm.yy
    'lesson_number': 2,
    'group_name': '8V92',
    'lesson_type': 'ПР',
}
"""


def create_lesson_from_dict(lesson_dict):
    date = datetime.datetime.strptime(lesson_dict['date'], '%Y-%m-%d')
    date = date.date()
    teacher = get_teacher_or_create_if_not_exist(lesson_dict['teacher_name'])
    subject = get_subject_or_create_if_not_exist(lesson_dict['full_name'],
                                                 lesson_dict['short_name'])
    group = get_group_or_create_if_not_exist(lesson_dict['group'])
    lesson_number = lesson_dict['lesson_number']
    lesson_type = lesson_dict['lesson_type']
    return Lesson(
        subject=subject,
        group=group,
        teacher=teacher,
        lesson_number=lesson_number,
        date=date,
        type=lesson_type
    )


def append_lessons_from_csv_file(file_pth):
    with open(file_pth, 'r', encoding='utf-8') as file:
        lessons = DictReader(file)
        lessons_created = 0
        lessons_total_created = 0
        lessons_to_create = []

        for lesson_dict in lessons:
            lessons_to_create.append(
                create_lesson_from_dict(lesson_dict)
            )
            lessons_created += 1
            lessons_total_created += 1
            print(lessons_total_created)

            if lessons_created == 25000:
                Lesson.objects.bulk_create(
                    lessons_to_create,
                    ignore_conflicts=True,
                )
                lessons_to_create = []
                lessons_created = 0

        if lessons_to_create:
            Lesson.objects.bulk_create(
                lessons_to_create,
                ignore_conflicts=True,
            )

    print('Finished adding lessons')


class Command(BaseCommand):
    # python manage.py add_lessons_from_csv lessons.csv
    help = "Import lesson from json file to database"

    def add_arguments(self, parser):
        parser.add_argument('csv_path', type=str)

    def handle(self, *args, **options):
        try:
            append_lessons_from_csv_file(options['csv_path'])
            self.stdout.write(self.style.SUCCESS("Success"))
        except FileNotFoundError:
            self.stdout.write(self.style.ERROR(f"No such file {options['csv_path']}"))
