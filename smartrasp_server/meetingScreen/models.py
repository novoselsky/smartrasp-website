from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
# django's settings needs for use Django's Schedule model as a foreign key
from django.conf import settings

import datetime


# Create your models here.


class Teacher(models.Model):
    full_name = models.CharField(max_length=150, unique=True)
    last_link = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.full_name


class Subject(models.Model):
    full_name = models.CharField(max_length=300, unique=True)
    short_name = models.CharField(max_length=100)

    def __str__(self):
        return self.short_name


class Group(models.Model):
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name


class Lesson(models.Model):
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE,
                                null=True, blank=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    date = models.DateField(null=False, blank=False)
    lesson_number = models.PositiveIntegerField(
        "Lesson's number in the day (from 1 to 7)",
        validators=[MinValueValidator(1), MaxValueValidator(8)])
    type = models.CharField(max_length=10, null=False, blank=False,
                            default='ПР')

    class Meta:
        unique_together = (('teacher', 'subject', 'group', 'date',
                            'lesson_number', 'type'),)


class Schedule(models.Model):
    django_user_ref = models.OneToOneField(settings.AUTH_USER_MODEL,
                                           on_delete=models.CASCADE,
                                           null=False, blank=False)
    main_group = models.ForeignKey(Group, on_delete=models.CASCADE,
                                   null=True, blank=True)
    main_teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE,
                                     null=True, blank=True)
    STUDENT = 0
    TEACHER = 1
    schedule_type_choices = (
        (STUDENT, 'Student'),
        (TEACHER, 'Teacher'),
    )
    schedule_type = models.IntegerField(default=0,
                                        choices=schedule_type_choices)

    def __str__(self):
        return str(self.id)

    def clean(self):
        """We should check if we adding user with correct fields
        filling. Student must have main group and must not have main
        teacher. Same for the teacher"""
        # Check for correct filling for student
        if self.schedule_type == self.STUDENT:
            if self.main_group is None:
                raise ValidationError('Student must have main group')
            if self.main_teacher is not None:
                raise ValidationError('Student must not have main teacher')
        # Check for correct filling for teacher
        if self.schedule_type == self.TEACHER:
            if self.main_teacher is None:
                raise ValidationError('Teacher must have main teacher')
            if self.main_group is not None:
                raise ValidationError('Teacher must not have main group')


class WeekDay(models.Model):
    day_name_choices = (
        (1, "Monday"),
        (2, "Tuesday"),
        (3, "Wednesday"),
        (4, "Thursday"),
        (5, "Friday"),
        (6, "Saturday"),
    )
    day_name = models.IntegerField(default=1, choices=day_name_choices,
                                   null=False, blank=False, unique=True)


class FilterInterface:
    """Using this interface you MUST create filter values dictionary.
    Filter values dict have next format: {"lessons_field": filter_value}
    """

    def filter_hide(self, lessons_set):
        """Returns lessons set without hide lessons"""
        filter_values = self.get_filter_values()
        # You must return empty array, showing that there is no
        # shadow filtered Lesson objects
        return lessons_set.exclude(**filter_values), lessons_set.none()

    def filter_shadow(self, lessons_set):
        """Returns two lessons set, with common and with shadow
        lessons"""
        filter_values = self.get_filter_values()
        shadow_lessons_set = lessons_set.filter(**filter_values)
        common_lessons_set = lessons_set.difference(shadow_lessons_set)
        return common_lessons_set, shadow_lessons_set

    def filter(self, lessons_set):
        if self.type == self.HIDE:
            return self.filter_hide(lessons_set)
        else:
            return self.filter_shadow(lessons_set)


class BaseFilter(FilterInterface, models.Model):
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE,
                                 null=False, blank=False)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE,
                                null=False, blank=False)
    HIDE = 0
    SHADOW = 1
    type_choices = (
        (HIDE, "Hide"),
        (SHADOW, "Shadow")
    )
    type = models.IntegerField(default=0, choices=type_choices,
                               null=False, blank=False)

    class Meta:
        abstract = True


class FilterSubjectName(BaseFilter):
    """Simple filter based on subject name"""

    def get_filter_values(self):
        filter_values = {
            'subject': self.subject
        }
        return filter_values
