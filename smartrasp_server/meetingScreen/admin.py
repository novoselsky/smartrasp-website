from django.contrib import admin
from .models import Teacher, Subject, Group, Lesson, Schedule
from .models import FilterSubjectName

# Register your models here.

schedule_models = [Teacher, Subject, Group, Lesson, Schedule,
                   FilterSubjectName]

admin.site.register(schedule_models)