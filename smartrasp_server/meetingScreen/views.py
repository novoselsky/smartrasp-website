from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.models import User as Django_User
from django.contrib.auth import authenticate, login, logout

from .week_handler import get_week_days_with_dates, get_current_week
from .week_handler import get_filtered_week_lessons_for_view

from .models import Schedule, Group, Teacher, FilterSubjectName
from .models import Subject

from random import randint


def index_page(request):
    if request.user.is_authenticated:
        cur_week = get_current_week()
        return HttpResponseRedirect(reverse('meetingScreen:table',
                                            args=(cur_week,)))
    context = {
        "groups_list": Group.objects.all(),
        "teachers_list": Teacher.objects.all(),
        "random_int": randint(1, 999999),
    }
    return render(request, 'meetingScreen/index.html', context)


def table_page(request, week_number):
    if week_number < 1:
        week_number = 1
    week_number = week_number - 1
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('meetingScreen:index'))
    user = Schedule.objects.get(django_user_ref=request.user)
    filtered_week_lessons_with_times = get_filtered_week_lessons_for_view(
        user, week_number)
    print(filtered_week_lessons_with_times)
    context = {
        'filtered_week_lessons_with_time': filtered_week_lessons_with_times,
        'week_days_and_dates': get_week_days_with_dates(week_number),
        'week_number': week_number,
        'profile': Schedule.objects.get(django_user_ref=request.user),
        'random_int': randint(1, 999999),
    }
    return render(request, 'meetingScreen/table.html', context)


def table_current_week(request):
    cur_week = get_current_week()
    return HttpResponseRedirect(reverse('meetingScreen:table',
                                        args=(cur_week,)))


def settings_page(request):
    user = request.user
    if not user.is_authenticated:
        return HttpResponseRedirect(reverse('meetingScreen:index'))
    context = {
        'profile': Schedule.objects.get(django_user_ref=request.user),
        'random_int': randint(1, 999999),
    }
    return render(request, 'meetingScreen/settings.html', context)


def change_password_action(request):
    current_week = get_current_week()
    return HttpResponseRedirect(reverse('meetingScreen:table',
                                        args=(current_week,)))


def create_and_return_new_django_user():
    new_django_user = Django_User.objects.create_user("new_user", "", "")
    new_django_user.username = new_django_user.id
    new_django_user.save()
    return new_django_user


def create_new_student(request):
    schedule_type = Schedule.STUDENT
    main_group = get_object_or_404(Group, name=request.POST['group'])
    new_django_user = create_and_return_new_django_user()
    new_user = Schedule(schedule_type=schedule_type,
                        main_group=main_group,
                        django_user_ref=new_django_user)
    new_user.save()
    login(request, new_django_user)
    cur_week = get_current_week()
    return HttpResponseRedirect(reverse('meetingScreen:table',
                                        args=(cur_week,)))


def create_new_teacher(request):
    schedule_type = Schedule.TEACHER
    main_teacher = get_object_or_404(Teacher, full_name=request.POST['teacher'])
    new_django_user = create_and_return_new_django_user()
    new_user = Schedule(schedule_type=schedule_type,
                        main_teacher=main_teacher,
                        django_user_ref=new_django_user)
    new_user.save()
    login(request, new_django_user)
    cur_week = get_current_week()
    return HttpResponseRedirect(reverse('meetingScreen:table',
                                        args=(cur_week,)))


def create_new_filter(request):
    schedule = get_object_or_404(Schedule, django_user_ref=request.user)
    subject_name = request.POST['subject']
    subject = get_object_or_404(Subject, full_name=subject_name)
    new_filter = FilterSubjectName(schedule=schedule, subject=subject)
    new_filter.save()
    next_page = request.POST.get('next', '/')
    return HttpResponseRedirect(next_page)


def login_view(request):
    username = str(request.POST['rasp_id'])
    password = request.POST['password']

    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)
        cur_week = get_current_week()

        return HttpResponseRedirect(reverse('meetingScreen:table',
                                            args=(cur_week,)))
    else:
        return HttpResponseRedirect(reverse('meetingScreen:index'))


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('meetingScreen:index'))
