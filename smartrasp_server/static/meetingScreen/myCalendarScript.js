// remember, mounth counts -1
let first_monday = new Date(2022, 7, 29);

function weeksBetween(d1, d2) {
    return Math.round((d2 - d1) / (7 * 24 * 60 * 60 * 1000));
}

$(function(){
  $("#calendar_container").simpleCalendar({

      months: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
      days: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],

      onDateSelect:function (date, events) {
        let weeks = weeksBetween(first_monday, date) + 1
        location.href="/table/" + weeks;
      },

      onMonthChange: setWeeks,
      onInit: setWeeksInit,

  });
});


function setWeeksInit(calendar) {
    month = calendar.currentDate.getMonth();
    year = calendar.currentDate.getFullYear();
    setWeeks(month, year)
}

function setWeeks(month, year) {
    let first_day = new Date(year, month, 1);
    let week_number = weeksBetween(first_monday, first_day) + 1
    $(".calendar thead").prepend("<td></td>");
    $(".calendar tr").each(function () {
        $(this).prepend('<th>' + week_number + '</th>');
        week_number++;
    })
}


$('#dropdownMenyButton2').on("click", function () {
    console.log(this);
})


function button_script() {
    lesson_name = $(this).siblings('.lesson').children('.lesson-name')[0].innerHTML;
    console.log(lesson_name);
    console.log('amogus');
    $('#filter_lesson_select').val(lesson_name);
    $('#filter_lesson_select').trigger('change');
}

$(document).ready(function() {
    // script for filter message pop up, when lesson is pressed
    let lesson_set = new Set();
    $('.lesson').each(function () {
        lesson_name = $(this).children('.lesson-name')[0].innerHTML;
        console.log
        lesson_set.add(lesson_name);
        let button = $(this).siblings('button');
        button.on("click", button_script);
    });
    let filter_lesson_select = $('#filter_lesson_select');
    lesson_set.forEach(function (lesson_name) {
        filter_lesson_select.append("<option value='" + lesson_name + "'>" + lesson_name + "</option>");
    });
    filter_lesson_select.select2();
    // If we would add this styles in css file
    // they wouldn't display, because select2
    // changing styles while init
    $(".select2-container--default .select2-selection--single").css("background-color", "#202124");
    $(".select2-container--default .select2-selection--single .select2-selection__rendered").css("color", "#bdc1c6");
    $(".select2-container--default .select2-selection--single .select2-selection__rendered").css("box-shadow", "1px 1px 15px 0px #171717");
    // We need to open and close our selects
    // to fix incorrect position showing
    // in a first open. It's this bug:
    // https://github.com/select2/select2/issues/3228
    filter_lesson_select.select2('open');
    filter_lesson_select.select2('close');
    filter_lesson_select.select2('open');
    filter_lesson_select.select2('close');
    filter_lesson_select.select2({
    sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
    });
});